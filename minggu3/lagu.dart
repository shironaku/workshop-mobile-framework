void main(List<String> args) async {
  print("mari bernyayi !!");
  print(await tes1());
  print(await line1());
  print(await line2());
  print(await line3());
  print(await line4());
  print(await line5());
  print(await line6());
  print(await line7());
  print(await line8());
  print(await line9());
  print(await line10());
}

Future<String> tes1() async {
  String greeting = "1,2,3 go";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}

Future<String> line1() async {
  String greeting = "Hei sephia";
  return await Future.delayed(Duration(seconds: 1), () => (greeting));
}

Future<String> line2() async {
  String greeting = "Malam ini ku takkan datang";
  return await Future.delayed(Duration(seconds: 3), () => (greeting));
}

Future<String> line3() async {
  String greeting = "Mencoba tuk berpaling sayang";
  return await Future.delayed(Duration(seconds: 4), () => (greeting));
}

Future<String> line4() async {
  String greeting = "Dari cintamu";
  return await Future.delayed(Duration(seconds: 3), () => (greeting));
}

Future<String> line5() async {
  String greeting = "Hei sephia";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line6() async {
  String greeting = "Malam ini ku takkan pulang";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}

Future<String> line7() async {
  String greeting = "Tak usah kau mencari aku";
  return await Future.delayed(Duration(seconds: 3), () => (greeting));
}

Future<String> line8() async {
  String greeting = "Demi cintamu";
  return await Future.delayed(Duration(seconds: 3), () => (greeting));
}

Future<String> line9() async {
  String greeting = "Hadapilah ini";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}

Future<String> line10() async {
  String greeting = "Kisah kita takkan abadi";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}
